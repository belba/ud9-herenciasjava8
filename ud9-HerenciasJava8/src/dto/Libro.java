package dto;

public class Libro {
	
	//Atributos
	private String isbn;
	private String titulo;
	private String autor;
	private int numero_paginas;
	
	//Constantes
	public Libro() {
		super();
	}


	public Libro(String isbn, String titulo, String autor, int numero_paginas) {
		super();
		this.isbn = isbn;
		this.titulo = titulo;
		this.autor = autor;
		this.numero_paginas = numero_paginas;
	}

	//Getters and Setters
	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}


	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}


	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}


	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	/**
	 * @return the autor
	 */
	public String getAutor() {
		return autor;
	}


	/**
	 * @param autor the autor to set
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}


	/**
	 * @return the numero_paginas
	 */
	public int getNumero_paginas() {
		return numero_paginas;
	}


	/**
	 * @param numero_paginas the numero_paginas to set
	 */
	public void setNumero_paginas(int numero_paginas) {
		this.numero_paginas = numero_paginas;
	}

	//Mostramos por pantalla el libro con su titulo, ISB, autor, n�mero paginas
	@Override
	public String toString() {
		
		return "El libro " + titulo + " con ISBN " + isbn + " creado por el autor " + autor + " tiene " + numero_paginas
				+ " p�ginas";
	}
	
	
	
	
	

}
