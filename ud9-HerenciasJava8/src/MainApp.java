import dto.Libro;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Libro l1 = new Libro("978-84-204-3938-9", "El enigma de la habitaci�n 622", "Dicker, Jo�l", 624);
		Libro l2 = new Libro("978-84-233-5756-7", "El mal de Corcira", "Silva, Lorenzo", 544);
		
		System.out.println(l1.toString());
		System.out.println(l2.toString());
		
		if(l1.getNumero_paginas() >= l2.getNumero_paginas()) {
			System.out.println("El libro 1 tiene mas p�ginas "+l1.getNumero_paginas());
		}else {
			System.out.println("El libro 2 tiene mas p�ginas "+l2.getNumero_paginas());
		}
	}

}
